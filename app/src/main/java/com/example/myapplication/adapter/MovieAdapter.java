package com.example.myapplication.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.myapplication.R;
import com.example.myapplication.model.Result;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

public class MovieAdapter extends RecyclerView.Adapter<MovieAdapter.NewsViewHolder> {

    Context context;
    List<Result> articles;

    public MovieAdapter(Context context, List<Result> articles) {
        this.context = context;
        this.articles = articles;
    }

    @NonNull
    @Override
    public MovieAdapter.NewsViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.single_layout, parent, false);
        return new  NewsViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MovieAdapter.NewsViewHolder holder, int position) {
    holder.tvName.setText(articles.get(position).getTitle().toString());
        //holder.tvName.setText(articles.get(position).toString());
        Picasso.get().load("https://image.tmdb.org/t/p/original/"+articles.get(position).getPosterPath()).into(holder.imageView);
    }

    @Override
    public int getItemCount() {
        return articles.size();
    }

    public class NewsViewHolder extends RecyclerView.ViewHolder{

        TextView tvName;

        ImageView imageView;

        public NewsViewHolder(@NonNull View itemView) {
            super(itemView);

            imageView  = itemView.findViewById(R.id.image);
            tvName = itemView.findViewById(R.id.text);

        }
    }
}
