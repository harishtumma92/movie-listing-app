package com.example.myapplication.viewmodel;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.example.myapplication.model.Response;
import com.example.myapplication.network.MovieRepository;

public class MovieViewModel extends ViewModel {

    private MutableLiveData<Response> mutableLiveData;
    private MovieRepository movieRepository;

    public void init(){
        if (mutableLiveData != null){
            return;
        }
        movieRepository = MovieRepository.getInstance();
        mutableLiveData = movieRepository.getMovies();

    }

    public LiveData<Response> getMovieList() {
        return mutableLiveData;
    }
}
