package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class LoginActivity extends AppCompatActivity {

    EditText emailTxt,passTxt;
    boolean isEmailValid=false;
    boolean isPasswordValid=false;
    Button loginbtn;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        emailTxt=findViewById(R.id.email_txt);
        passTxt=findViewById(R.id.pass_txt);
        loginbtn=findViewById(R.id.btn);
        enableBtn();

        emailTxt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                isEmailValid=validateEmail();
                enableBtn();
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        passTxt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                isPasswordValid=validatePass();
                enableBtn();
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        loginbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(LoginActivity.this,MainActivity.class);
                startActivity(i);
            }
        });

    }


    private void enableBtn(){
        if(isEmailValid && isPasswordValid)
        {
            loginbtn.setEnabled(true);
            loginbtn.setClickable(true);
        }
        else {
            loginbtn.setEnabled(false);
            loginbtn.setClickable(false);
        }
    }


    private boolean validateEmail()
    {
        String email=emailTxt.getText().toString().trim();
        if(email==null||email.equalsIgnoreCase(""))
        {
            emailTxt.setError("Invalid Email");
            return false;
        }
        String regex = "^[a-zA-Z0-9+_.-]+@[a-zA-Z0-9.-]+$";
        if(!email.matches(regex))
        {
            emailTxt.setError("Invalid Email");
            return false;
        }
        return true;
    }

    private boolean validatePass(){
        String pass=passTxt.getText().toString().trim();
        if(pass==null||pass.equalsIgnoreCase(""))
        {
            passTxt.setError("Invalid Password");
            return false;
        }
        if(pass.length()<6 ||pass.length()> 12 )
        {
            passTxt.setError("Invalid Password");
            return false;

        }
        return true;
    }
}