package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;

import com.example.myapplication.adapter.MovieAdapter;
import com.example.myapplication.model.Response;
import com.example.myapplication.model.Result;
import com.example.myapplication.viewmodel.MovieViewModel;

import java.util.List;

public class MainActivity extends AppCompatActivity {

    RecyclerView movieRecyclerview;
    MovieViewModel movieViewModel;
    MovieAdapter adapter;
    List<Result> movieList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        movieRecyclerview = findViewById(R.id.recyclerview);

        movieViewModel = ViewModelProviders.of(this).get(MovieViewModel.class);
        movieViewModel.init();



        movieViewModel.getMovieList().observe(this,movieResponse -> {
            movieList = movieResponse.getResults();
            setupRecyclerView();
        });

    }



    private void setupRecyclerView() {
        if (adapter == null) {
            adapter = new MovieAdapter(MainActivity.this, movieList);
            movieRecyclerview.setLayoutManager(new LinearLayoutManager(this));
            movieRecyclerview.setAdapter(adapter);
            movieRecyclerview.setItemAnimator(new DefaultItemAnimator());
            movieRecyclerview.setNestedScrollingEnabled(true);
            adapter.notifyDataSetChanged();
        } else {
            adapter.notifyDataSetChanged();
        }
    }
}