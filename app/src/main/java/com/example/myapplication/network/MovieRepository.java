package com.example.myapplication.network;

import androidx.lifecycle.MutableLiveData;

import com.example.myapplication.model.Response;

import retrofit2.Call;
import retrofit2.Callback;

public class MovieRepository {

    private static MovieRepository movieRepository;

    public static MovieRepository getInstance(){
        if (movieRepository == null){
            movieRepository = new MovieRepository();
        }
        return movieRepository;
    }

    private MovieApi movieApi;

    public MovieRepository(){
        movieApi = RetrofitService.cteateService(MovieApi.class);
    }

    public MutableLiveData<Response> getMovies( ){
        MutableLiveData<Response> movieData = new MutableLiveData<>();

        movieApi.getMovieList("726210383dac277582f103793203ec93","en-US","1").enqueue(new Callback<Response>() {
            @Override
            public void onResponse(Call<Response> call, retrofit2.Response<Response> response) {
                   if(response.isSuccessful())
                   {
                       movieData.setValue(response.body());
                   }
                   else {
                       movieData.setValue(null);
                   }
            }

            @Override
            public void onFailure(Call<Response> call, Throwable t) {

            }
        });

        return movieData;
    }
}
