package com.example.myapplication.network;

import com.example.myapplication.model.Response;

import retrofit2.Call;

import retrofit2.http.GET;
import retrofit2.http.Query;

public interface MovieApi {

    @GET("movie/popular")
    Call<Response> getMovieList(@Query("api_key") String apiKey,
                                @Query("language") String language,
                                @Query("page") String page);
}
